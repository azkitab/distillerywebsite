<?php

/**
 * Api Extenion for the website
 * @Author: A.K
 * @Company: Distillery
 * @ Version 1.0
 */
function add_cors_http_header(){
    header("Access-Control-Allow-Origin: *");
}
add_action('init','add_cors_http_header');

 /**Adding Primary menu to custom Rest API end point */
add_action( 'rest_api_init', function () {
	register_rest_route( 'website/v1', '/author/', array(
	  'methods' => 'GET',
	  'callback' => 'get_menu',
	) );
  } );

function get_menu() {
	$data = wp_get_nav_menu_items('Main Menu');
	$response = new WP_REST_Response($data, 200);
	$response->set_headers([ 'Cache-Control' => 'must-revalidate, no-cache, no-store, private' ]);
	return $response;
}





?>