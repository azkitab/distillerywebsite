<?php
/**
 * Header file for the Distillery WordPress default theme.
 *
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<header id="site-header" class="header-footer-group" role="banner">

			<div class="header-inner section-inner">

				<div class="header-titles-wrapper">

					

					
					<div class="header-titles">

						<?php
							// Site title or logo.
							twentytwenty_site_logo();

							// Site description.
							twentytwenty_site_description();
						?>

					</div><!-- .header-titles -->

					

				</div><!-- .header-titles-wrapper -->

				<div class="meanu-block">
					<div id="nav-icon1">
						<span></span>
						<span></span>
						<span></span>
					</div>
                    <div class="menu-text">
					   MENU
					</div>

			  </div>
			  
			</div><!-- .header-inner -->

			
		</header><!-- #site-header -->

	