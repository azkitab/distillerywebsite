  var gulp = require( 'gulp' ),
  plumber = require( 'gulp-plumber' ),
  watch = require( 'gulp-watch' ),
  livereload = require( 'gulp-livereload' ),
  minifycss = require( 'gulp-minify-css' ),
  jshint = require( 'gulp-jshint' ),
  stylish = require( 'jshint-stylish' ),
  uglify = require( 'gulp-uglify' ),
  rename = require( 'gulp-rename' ),
  notify = require( 'gulp-notify' ),
  include = require( 'gulp-include' ),
  sass = require( 'gulp-sass' );
  var concat = require('gulp-concat');
  var order = require('gulp-order');
  
var onError = function( err ) {
  console.log( 'An error occurred:', err.message );
  this.emit( 'end' );
}

gulp.task( 'scss', function() {
  return gulp.src( './src/scss/*.scss' )
    .pipe( plumber( { errorHandler: onError } ) )
    .pipe( sass() )
    .pipe( gulp.dest( './css' ) )
    .pipe( livereload() );
} );
gulp.task('concat', function() {
  return gulp.src('css/*.css')
      .pipe(order([
        'base.css',
        'bootstrap.css',
        'variables',
        'fonts.css',
        'place-holder.css',
        'structure.css',
        'innerpages.scss'
      ]))
      .pipe(concat('style.css'))
      .pipe(gulp.dest('.'));
});
gulp.task('watch', function(done) {
  // watch scss for changes and clear drupal theme cache on change
  gulp.watch(['./src/scss/*.scss'], gulp.series(['scss','concat']));
  
});


gulp.task('default', gulp.series(['scss','concat','watch']));