<?php
/**
 * Single Project Page Distillery
 * @Author : A.K
 */

get_header();
?>

<main id="site-content" role="main">
   <div class="header-space"></div>
	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			the_content();
		}
	}

	?>

</main><!-- #site-content -->


<?php get_footer(); ?>