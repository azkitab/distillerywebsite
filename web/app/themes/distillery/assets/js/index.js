/*	-----------------------------------------------------------------------------------------------
	Custom Javascript
	@ Author A.K
	@ Distillery
--------------------------------------------------------------------------------------------------- */

  $(document).ready(function(){

    var container = $('.container');
	$('.grid').isotope({
		// options
		itemSelector: '.grid-item',
		layoutMode: 'masonry',
		masonry: {
			gutter: 5,
            horizontalOrder: true,
            
		  }
	  });

    /**
	 * Remove Link from Client logos's 
	 * Disable this this code snippet if you need to link the logos to client page
	 */
	  $(".clients-logo a").each(function() {
		  $(this).removeAttr('href'); 
	   })
	   

  })


var canvas = document.getElementById('art-lines');
if(canvas!=null)
{
var width = canvas.offsetWidth,
    height = canvas.offsetHeight;

var renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true,
    alpha: true
});
renderer.setPixelRatio(window.devicePixelRatio > 1 ? 2 : 1);
renderer.setSize(width, height);
renderer.setClearColor(0x000000,0);

var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(40, width / height, 0.1, 1000);
camera.position.set(-90, 0, 350);

var sphere = new THREE.Group();
scene.add(sphere);
var material = new THREE.LineBasicMaterial({
    color: 0xE97504
});
var linesAmount = 3;
var radius = 130;
var verticesAmount = 100;
for(var j=0;j<linesAmount;j++){
    var index = j;
    var geometry = new THREE.Geometry();
    geometry.y = (index/linesAmount) * radius*2;
    for(var i=0;i<=verticesAmount;i++) {
        var vector = new THREE.Vector3();
        vector.x = Math.cos(i/verticesAmount * Math.PI*2);
        vector.z = Math.sin(i/verticesAmount * Math.PI*2);
        vector._o = vector.clone();
        geometry.vertices.push(vector);
    }
    var line = new THREE.Line(geometry, material);
    sphere.add(line);
}

function updateVertices (a) {
 for(var j=0;j<sphere.children.length;j++){
     var line = sphere.children[j];
     line.geometry.y += 0.3;
     if(line.geometry.y > radius*2) {
         line.geometry.y = 0;
     }
     var radiusHeight = Math.sqrt(line.geometry.y * (2*radius-line.geometry.y));
     for(var i=0;i<=verticesAmount;i++) {
         var vector = line.geometry.vertices[i];
            var ratio = noise.simplex3(0, vector.z*0.009 + a*0.0006, line.geometry.y*0.0) * 15;
            vector.copy(vector._o);
            vector.multiplyScalar(radiusHeight + ratio);
            vector.y = 10;
        }
     line.geometry.verticesNeedUpdate = true;
 }

 
}

function render(a) {
    requestAnimationFrame(render);
    updateVertices(a);
    renderer.render(scene, camera);
    TweenMax.to(sphere.rotation, 2, {
        x : (2 * 1),
        ease:Power1.easeOut
    });
    
}

function onResize() {
    canvas.style.width = '';
    canvas.style.height = '';
    width = canvas.offsetWidth;
    height = canvas.offsetHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
}

//var mouse = new THREE.Vector2(0.8, 0.5);

function onMouseMove(e) {
  //  mouse.y = e.clientY / window.innerHeight;
   // TweenMax.to(sphere.rotation, 2, {
   //     x : (mouse.y * 1),
   //     ease:Power1.easeOut
   // });
    //material.color = new THREE.Color(0xffffff * Math.random());

}

requestAnimationFrame(render);
window.addEventListener("mousemove", onMouseMove);
var resizeTm;
window.addEventListener("resize", function(){
    resizeTm = clearTimeout(resizeTm);
    resizeTm = setTimeout(onResize, 200);
});

}
/************************************************************************************************
 *  Load SVG image in Konvas and render it to canvas
 *  Moves Lines on mouse over
 * 
 *************************************************************************************************/



    var width = window.innerWidth;
    var height = window.innerHeight;

    var lines_div = document.getElementById('lines');  
 
   if(lines_div!=null)
   {

    var stage = new Konva.Stage({
      container: 'lines',
      width: width,
      height: height,
     
    });
    
    window.addEventListener('resize', fitStageIntoParentContainer);

    function fitStageIntoParentContainer()
    {
        stage.width(width);
        stage.height(height);
        stage.draw();
        
    }

    var layer = new Konva.Layer();

     
      var theImage = new Image();
      var theImage2 = new Image();
      theImage.src = "http://distillery.local/app/uploads/2020/01/dst-science-line.svg";
      theImage2.src = "http://distillery.local/app/uploads/2020/01/dst-science-line.svg";  
        
            var line = new Konva.Image({
                image: theImage2,
                x: width/2,
                y: height/2,
              });
              layer.add(line);
              stage.add(layer);
                         
            var line_2 = new Konva.Image({
                image: theImage,
                x: $('.right-section .background-image-wrapper').width(),
                y: 0,
                
                
              });
              line.rotation(90);
              line.zIndex(1);
              layer.add(line_2);
              stage.add(layer);
 
             
            stage.on('mousemove', function(e) {
                var pos = getRelativePointerPosition(layer);
                //line.x(pos.x);
                line.y(pos.y);
                
               
                line_2.position({
                    x: pos.x,
                    
                  });
                //layer.add(line);
                layer.add(line_2);
              stage.add(layer);
              
              });

              function getRelativePointerPosition(node) {
                // the function will return pointer position relative to the passed node
                var transform = node.getAbsoluteTransform().copy();
                // to detect relative position we need to invert transform
                transform.invert();
        
                // get pointer (say mouse or touch) position
                var pos = node.getStage().getPointerPosition();
        
                // now we find relative point
                return transform.point(pos);
              }

   //Move Lines section Closed
   
   /********************************************************************************
    * Meet Section Dynamic width and adjustment to the center of the screen
    **********************************************************************************/
   
    
   var positionadjust = {width: 120, top: 40, left:84,height:120};
   var meetpeople = {
       person1:{
         image:'http://distillery.local/app/uploads/2020/01/meet1.png',
         name:'John Doe',
         link:'#hihih'
       },
       person2:{
        image:'http://distillery.local/app/uploads/2020/01/meet2.png',
        name:'John Half',
        link:'#hihiwdwqdh'
      }
   }
  
    function setmeetcontainer()
    {      
        var width_right = $('.right-section .background-image-wrapper').width();
        var height_right = $('.right-section .background-image-wrapper').height();
        var left_right = $('.left-section .background-image-wrapper').width();

        var total_width= width_right + left_right-positionadjust.width+20;
  
        $('.meetsection').width(total_width);
        $('.meetsection').height(height_right-positionadjust.height);
        var pos = $('.left-section .background-image-wrapper').position();
        $(".meetsection").css({top: pos.top+positionadjust.top, left: pos.left+positionadjust.left});
    }

    window.onresize = function(event) {
        setmeetcontainer();
    };

    
    $(window).bind("load", function () {
        setmeetcontainer();
    });
    

    $(document).ready(function(){
        
        $('.meet-click').click(function(){
        
            var tl = new TimelineMax();
            tl.pause();
            var f1 = TweenMax.to('.meet-click', 0, {top:'73%', repeat: 0, ease: Power0.easeNone,});
            var f2 =TweenMax.to('.meet-left img', 2, {opacity:1, ease:Power3.easeOut});
            var f3 = TweenMax.to('.meet-right img', 2, {opacity:1, ease:Power3.easeOut});
            var f4 = TweenMax.to('.meet-left div.title', 2, {opacity:1, ease:Power3.easeOut});
            var f5 = TweenMax.to('.meet-left a', 2, {opacity:1, ease:Power3.easeOut});
            var f6 = TweenMax.to('.meet-right div.title', 2, {opacity:1, ease:Power3.easeOut});
            var f7 = TweenMax.to('.meet-right a', 2, {opacity:1, ease:Power3.easeOut});

            $('.meet-left img').attr('src',meetpeople.person2.image);
            $('.meet-left div.title').text(meetpeople.person2.name);
            $('.meet-left a').attr('href',meetpeople.person2.link);
            $('.meet-right img').attr('src',meetpeople.person1.image);
            $('.meet-right div.title').text(meetpeople.person1.name);
            $('.meet-right a').attr('href',meetpeople.person1.link);
            tl.add(f1,0.3);
            tl.add(f2,2);
            tl.add(f3,3);
            tl.add(f4,4);
            tl.add(f5,5);
            tl.add(f6,6);
            tl.add(f7,7);
            tl.play();

        })

    });
}


    /**
     * Custom Menu 
     */
    $ = jQuery.noConflict();
    $(document).ready(function(){
        $('#nav-icon1').click(function(){
           
            $(this).toggleClass('open');
            $('.menu-modal-container').toggleClass('showMenu');
        });
    });
    
  
    $(document).ready(function($){
        $('#client-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2
                }
            }]
        });
    });
    