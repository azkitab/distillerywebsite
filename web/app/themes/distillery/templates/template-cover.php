<?php
/**
 * Template Name: Home Page
 * 
 *
 */

get_header();
?>

<main id="site-content" role="main">

	<div class="home-page-hero-wrapper">

	  <div class="left-section">
	 
		  <div class="background-image-wrapper">
		  
		     <img src="<?php echo  get_template_directory_uri()."/assets/images/laptop.png" ?>"> 
		  </div>
		  <canvas id="art-lines" resize></canvas>
	  </div>

	  <div class="right-section"> 
	     <div class="background-image-wrapper">
		     <img src="<?php echo  get_template_directory_uri()."/assets/images/mobile.png" ?>"> 
			 
		   </div>
			  <div id="lines"></div>
	  </div>
	  
	 <div class="heading-wrapper">
         <div class="inner-wrapper">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<h3>ART,</h3>
						</div>
						<div class="col-sm-4 white-text-center">
						    
						</div>
						<div class="col-sm-4 black-text-right">
                             <h3>SCIENCE</h3>
						</div>
					</div>
				</div>
			</div>
		 </div>
	</div>

	
	<div class="meetsection">
		<h3 class="meet-click">&lt;MEETS/&gt;</h3>
		<div class="meet-right">
			<img />
			<div class='title'> </div>
			 <a>click here</a> 
		</div>
        <div class="meet-left">
		     <img />
			<div class='title'></div>
			<a>click here</a> 
		</div>
	</div>

 <!----------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------
					  Home Page Content section from Wordpress Blocks
-------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------->					  

	<div class="content-wrapper">
	   
	   <?php if( have_posts() ): while( have_posts() ): the_post();
					$content = get_the_content();
					echo apply_filters('the_content',$content); 
					  endwhile;
			endif;
		?>
	  
	</div>

 <!----------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------
					  Home Page Content section from Wordpress Blocks Close
-------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------->	

</main><!-- #site-content -->


<?php get_footer(); ?>
