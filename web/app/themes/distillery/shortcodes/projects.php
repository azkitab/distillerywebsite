<?php

function projects_shortcode() {

$html="
<div class='post-container'>
  <div class='container'>
   <div class='grid'>
      <div class='grid-sizer'></div>
      <div class='gutter-sizer'></div>
      <div class='grid-item text-block'>//<br>
     Recent Work ------------------------------------------- //
     </div>";
        
        $args = array( 'post_type' => 'projects', 'posts_per_page' => 10 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
        
        
       $html .= '<div class="grid-item"><a href="'.get_the_permalink().'"> <img src="'.get_the_post_thumbnail_url().'"></a></div>';

         
endwhile;
$html.= "</div></div></div>";
return $html;

}
add_shortcode( 'projects', 'projects_shortcode' );

?>