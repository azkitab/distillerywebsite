<?php

/**
 * Contact Shortcode
 * @Author : A.K 
 */

add_shortcode( 'contactusblock', 'contact_shortcode' );


function contact_shortcode() {

	$html="
	<div class='contact-block'>
	   <div class='conatiner'>
		  <div class='row'>
			 <div class='col-12 col-md-4 col-lg-4 no-padding' >
				<img src='".get_template_directory_uri()."/assets/images/dst-computer.png'>
				
				<div class='black-overlay'>
					<div class='inner-text-holder'>
						<span class='code-orange'>&lt;h2/&gt;</span> <span class='code-white'>Say Hi</span>  <span class='code-orange'>&lt;/h2&gt;</span>
					
						<div class='ph-item'>
						<div class='ph-col-12'>
							
							<div class='ph-row'>
								<div class='ph-col-2'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-4'></div>
								<div class='ph-col-2'></div>
							</div>

							<div class='ph-row'>
								<div class='ph-col-4'></div>
								<div class='ph-col-2'></div>
							</div>
							<div class='ph-row'>
								 <div class='ph-col-2'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-12'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-3'></div>
								<div class='ph-col-2'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-6'></div>
								<div class='ph-col-2'></div>
							</div>

							<div class='ph-row'>
								<div class='ph-col-2'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-4'></div>
								
							</div>
							
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-6'></div>
								
							</div>

							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-2'></div>
								<div class='ph-col-2'></div>
								<div class='ph-col-2'></div>
								
							</div>
						   
							<div class='ph-row'>
								<div class='ph-col-10'></div>
							</div>

							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-4'></div>
								<div class='ph-col-2'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-4'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-2'></div>
							</div>

							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-4'></div>
							</div>

							<div class='ph-row'>
								<div class='ph-col-8'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-2'></div>
								
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-1'></div>
								<div class='ph-col-2'></div>
							</div>
							<div class='ph-row'>
								<div class='ph-col-1'></div>
								<div class='ph-col-2'></div>
								<div class='ph-col-2'></div>
								<div class='ph-col-2'></div>
							</div>

						</div>
					</div>


					
					</div>
				</div>
			 </div>

			 <div class='col-12 col-md-8 col-lg-8 bg-color'>
			   <div class='contant-form-container'>
				<h4>// Talk to us about your next project<br>
				----------------------------------------------//</h4>
				 
				 <div class='form'>
					  ".do_shortcode('[gravityform id="1"  title="false" description="false" ajax="true"]')."
				 </div>

			   </div>	
			 </div>
		  </div>
	   </div> 
	 </div>   
	";

	return $html;

}

?>