<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class="footer-credits">

						<p class="footer-copyright">
							
							<span class="address">
								   <span>tel:</span> (08) 8120 4888<br>
								   <span>addr: </span>39 Rundle St, Kent Town SA 5067</span>
							   </span>
						</p><!-- .footer-copyright -->

						

					</div><!-- .footer-credits -->

					
						<span class="footer to-the-top-long">
						               THE DISTILLERY
						</span><!-- .to-the-top-long -->
						
					

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->


			<?php 
			get_template_part('/template-parts/modal-menu')
			
			?>


		<?php wp_footer(); ?>





	</body>
</html>
